package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A1MainPage {
    public By UserName = By.id("login-username-input");
    public By Password = By.id("login-password-input");
    public By SignIn_Btn = By.id("login-button");
    WebDriver driver;

    public A1MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void IVPS_SignIn() {
        ElementActions.type(driver, UserName, "super");
        ElementActions.type(driver, Password, "super");
        ElementActions.click(driver, SignIn_Btn);
    }
}

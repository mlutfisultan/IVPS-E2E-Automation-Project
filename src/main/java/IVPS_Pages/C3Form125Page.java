package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.Random;

public class C3Form125Page {
    public JavascriptExecutor jse;
    public By Form_125_Generator_Btn = By.id("pages-entry-form125-generator");
    public By Form_125_Radar_Code_Btn = By.id("violations-filter-parameters-radarcode-input");
    public By Form_125_Arrangement_Type = By.id("violations-filter-parameters-sort-type-input");// Drop Menu
    public By Form_125_Arrange_Type_ASC = By.id("violations-filter-parameters-sort-type-input-option-asc");
    public By Form_125_Arrange_Type_DSC = By.id("violations-filter-parameters-sort-type-input-option-desc");
    public By Form_125_Violation_From_Date_Input = By.cssSelector("#violations-filter-parameters-violation-date-input .p-inputtext");
    public By Form_125_Review_Activation = By.id("violations-filter-parameters-review-form-input");
    public By Form_125_Review_Activation_Yes = By.id("violations-filter-parameters-review-form-input-option-yes");
    public By Form_125_Review_Activation_No = By.id("violations-filter-parameters-review-form-input-option-no");
    public By Form_125_View_Violations_Btn = By.id("violations-filter-parameters-show-violations-button");
    public By Form_125_Cancel_Btn = By.id("violations-filter-parameters-cancel-button");
    public By Form_125_old_Plate_ID_CBox = By.id("form125-generator-oldPlateNumber-input");
    public By Form_125_Cities_Menu = By.id("form125-generator-governorates-input");
    public By Form_125_old_Paintings_CitySearch = By.cssSelector("p-dropdownitem:nth-child(14) > .p-dropdown-item");
    public By Form_125_Plate_Source_Locator = By.cssSelector("div.move");
    public By Form_125_Plate_Destination_Locator = By.cssSelector("img.source-image.ng-star-inserted");
    public By Form_125_Plate_ID_1 = By.id("cpn1");
    public By Form_125_Plate_ID_2 = By.id("cpn2");
    public By Form_125_Plate_ID_3 = By.id("cpn3");
    public By Form_125_Plate_ID_4 = By.id("cpn4");
    public By Form_125_Plate_ID_5 = By.id("cpn5");
    public By Form_125_Type_of_Offense = By.cssSelector("#form125-generator-select-violations-input .p-multiselect-trigger");// Drop Menu
    public By Form_125_Type_Offense_Child1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child5 = By.cssSelector("p-multiselectitem:nth-child(5)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child6 = By.cssSelector("p-multiselectitem:nth-child(6)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child7 = By.cssSelector("p-multiselectitem:nth-child(7)  > .p-multiselect-item");
    public By Form_125_Type_Offense_Child8 = By.cssSelector("p-multiselectitem:nth-child(8)  > .p-multiselect-item");
    public By Offenses_List_closer_Btn = By.cssSelector(".p-multiselect-close-icon");
    public By Form_125_Creation_Btn = By.id("form125-generator-submit-form125-button");
    public By Form_125_Full_View_Image_Btn = By.id("form125-generator-view-full-image-button");
    public By Form_125_Skip_Violation_Btn = By.id("form125-generator-skip-violation-button");
    public By Form_125_Vehicle_Image_Processing_Btn = By.id("image-cropper-view-cropped-image-button");
    WebDriver driver;

    public C3Form125Page(WebDriver driver) {
        this.driver = driver;
        jse = (JavascriptExecutor) driver;
    }

    public void Select_Form_125_Generator_Btn() {
        ElementActions.click(driver, Form_125_Generator_Btn);
    }

    public void Select_Form_125_Radar_Code_Btn() {
        ElementActions.click(driver, Form_125_Radar_Code_Btn);
    }

    public void Select_Form_125_Arrangement_Type() {
        ElementActions.click(driver, Form_125_Arrangement_Type);
    }

    public void Select_Form_125_Arrangement_Type_Asc() {
        ElementActions.click(driver, Form_125_Arrange_Type_ASC);
    }

    public void Select_Form_125_Arrangement_Type_Dsc() {
        ElementActions.click(driver, Form_125_Arrange_Type_DSC);
    }

    public void Type_Form_125_Violation_From_Date() {

        ElementActions.type(driver, Form_125_Violation_From_Date_Input, "2020-01-01");
    }

    public void Select_Form_125_Review_Activation() {
        ElementActions.click(driver, Form_125_Review_Activation);
    }

    public void Select_Form_125_Review_Activation_Yes() {
        ElementActions.click(driver, Form_125_Review_Activation_Yes);
    }

    public void Select_Form_125_Review_Activation_No() {
        ElementActions.click(driver, Form_125_Review_Activation_No);
    }

    public void Select_Form_125_View_Violations_Btn() {
        ElementActions.click(driver, Form_125_View_Violations_Btn);
    }

    public void Select_Form_125_Cancel_Btn() {
        ElementActions.click(driver, Form_125_Cancel_Btn);
    }

    public void Select_Form_125_old_Plate_ID_CBox() {
        ElementActions.click(driver, Form_125_old_Plate_ID_CBox);
    }

    public void Select_Form_125_Cities_Menu() {
        ElementActions.click(driver, Form_125_Cities_Menu);
    }

    public void Search_Form_125_old_Paintings_City() {
        ElementActions.click(driver, Form_125_old_Paintings_CitySearch);
    }

    public void Drag_Plate_Identifier() {
        ElementActions.dragAndDrop(driver, Form_125_Plate_Source_Locator, Form_125_Plate_Destination_Locator);
    }

    public void Type_Form_125_Plate_ID_1() {
        ElementActions.type(driver, Form_125_Plate_ID_1, getRandomCharacter());
    }

    public void Type_Form_125_Plate_ID_2() {
        ElementActions.type(driver, Form_125_Plate_ID_2, getRandomCharacter());
    }

    public void Type_Form_125_Plate_ID_3() {
        ElementActions.type(driver, Form_125_Plate_ID_3, getRandomCharacter());
    }

    public void Type_Form_125_Plate_ID_4() {
        ElementActions.type(driver, Form_125_Plate_ID_4, getRandomCharacter());
    }

    public void Type_Form_125_Plate_ID_5() {

        ElementActions.type(driver, Form_125_Plate_ID_5, getRandomNumber());
    }

    public void Type_Form_125_Plate_ID() {
        ElementActions.type(driver, Form_125_Plate_ID_1, getRandomCharacter());
        ElementActions.type(driver, Form_125_Plate_ID_2, getRandomCharacter());
        ElementActions.type(driver, Form_125_Plate_ID_3, getRandomCharacter());
//        ElementActions.type(driver, Form_125_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, Form_125_Plate_ID_5, getRandomNumber());
    }

    public void Select_Form_125_Type_of_Offense() {
        ElementActions.click(driver, Form_125_Type_of_Offense);
    }

    public void Select_Form_125_Type_Offense_Child1() {
        ElementActions.click(driver, Form_125_Type_Offense_Child1);
    }

    public void Select_Form_125_Type_Offense_Child2() {
        ElementActions.click(driver, Form_125_Type_Offense_Child2);
    }

    public void Select_Form_125_Type_Offense_Child3() {
        ElementActions.click(driver, Form_125_Type_Offense_Child3);
    }

    public void Select_Form_125_Type_Offense_Child5() {
        ElementActions.click(driver, Form_125_Type_Offense_Child5);
    }

    public void Select_Form_125_Type_Offense_Child6() {
        ElementActions.click(driver, Form_125_Type_Offense_Child6);
    }

    public void Select_Form_125_Type_Offense_Child7() {
        ElementActions.click(driver, Form_125_Type_Offense_Child7);
    }

    public void Select_Form_125_Type_Offense_Child8() {
        ElementActions.click(driver, Form_125_Type_Offense_Child8);
    }

    public void Select_List_Of_Form_125_Type_Offense_Child() {
        ElementActions.click(driver, Form_125_Type_Offense_Child1);
        ElementActions.click(driver, Form_125_Type_Offense_Child2);
        ElementActions.click(driver, Form_125_Type_Offense_Child3);
        ElementActions.click(driver, Form_125_Type_Offense_Child5);
        /*ElementActions.click(driver, Form_125_Type_Offense_Child6);
        ElementActions.click(driver, Form_125_Type_Offense_Child7);
        ElementActions.click(driver, Form_125_Type_Offense_Child8);*/
    }

    public void Select_Offenses_List_closer_Btn() {
        ElementActions.click(driver, Offenses_List_closer_Btn);
    }

    public void Select_Form_125_Creation_Btn() {
        ElementActions.hoverAndClick(driver, Form_125_Creation_Btn, Form_125_Creation_Btn);
    }

    public void Select_Form_125_Full_View_Image_Btn() {
        ElementActions.click(driver, Form_125_Full_View_Image_Btn);
    }

    public void Select_Form_125_Skip_Offense_Btn() {
        ElementActions.click(driver, Form_125_Skip_Violation_Btn);
    }

    public void Select_Form_125_Vehicle_Image_Processing_Btn() {
        ElementActions.click(driver, Form_125_Vehicle_Image_Processing_Btn);
    }

    public void Fill_inputs_View_Violartions() {
        ElementActions.click(driver, Form_125_Generator_Btn);
        ElementActions.click(driver, Form_125_Arrangement_Type);
        ElementActions.click(driver, Form_125_Arrange_Type_DSC);
        ElementActions.type(driver, Form_125_Violation_From_Date_Input, "2020-01-01");
        ElementActions.click(driver, Form_125_View_Violations_Btn);
    }

    public void Create_125_Form_old_Plate_ID() {
        ElementActions.click(driver, Form_125_old_Plate_ID_CBox);
        ElementActions.click(driver, Form_125_Cities_Menu);
        ElementActions.click(driver, Form_125_old_Paintings_CitySearch);
        ElementActions.type(driver, Form_125_Plate_ID_5, "1234");
        ElementActions.click(driver, Form_125_Type_of_Offense);
        Select_List_Of_Form_125_Type_Offense_Child();
        ElementActions.hoverAndClick(driver, Form_125_Creation_Btn, Form_125_Creation_Btn);
    }

    public void Create_125_Form_New_Plate_ID() {
        ElementActions.dragAndDrop(driver, Form_125_Plate_Source_Locator, Form_125_Plate_Destination_Locator);
        Type_Form_125_Plate_ID();
        ElementActions.click(driver, Form_125_Type_of_Offense);
        Select_List_Of_Form_125_Type_Offense_Child();
        ElementActions.click(driver, Offenses_List_closer_Btn);
        ElementActions.hoverAndClick(driver, Form_125_Creation_Btn, Form_125_Creation_Btn);
    }
    public String getRandomCharacter() {
        Random random = new Random();
        return ((Character) (char) random.ints(1575, 1610).findAny().getAsInt()).toString();
    }

    public String getRandomNumber() {
        Random random = new Random();
        return random.ints(100, 9999).findAny().getAsInt() + "";
    }

}

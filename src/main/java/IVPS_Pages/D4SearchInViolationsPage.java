package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class D4SearchInViolationsPage {
    public By Search_Violations_Btn = By.id("pages-entry-violations-manager");
    public By Search_For_Violations_Label = By.cssSelector("#breadcrumb-violations-manager > span");
    public By Search_For_Violations_By_Plate_Number = By.id("form-control-plateNumber-input");
    public By Search_For_Violations_By_Code = By.id("form-control-violationCode-input");
    public By Search_For_Violations_By_Username = By.id("form-control-userName-input");
    public By Search_For_Violations_By_Status_List = By.cssSelector("#form-control-status-input .p-multiselect-label");
    public By Search_For_Violations_Status_1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By Search_For_Violations_Status_2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By Search_For_Violations_Status_3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By Search_For_Violations_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Search_For_Violations_Search_Result = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    WebDriver driver;

    public D4SearchInViolationsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Serach_Violations_Btn() {
        ElementActions.click(driver, Search_Violations_Btn);
    }

    public void Search_By_New_Plate_ID(String PlateID) {
        ElementActions.type(driver, Search_For_Violations_By_Plate_Number, PlateID);
        ElementActions.click(driver, Search_For_Violations_Search_Btn);
    }

    public void Search_By_Old_Plate_ID(String PLateID) {
        ElementActions.type(driver, Search_For_Violations_By_Plate_Number, PLateID);
        ElementActions.click(driver, Search_For_Violations_Search_Btn);
    }
}

package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class E5AutoCreateModel125Page {
    public By Auto_Create_125_Model_Btn = By.id("pages-entry-automatic-generation");
    public By Auto_Create_125_Model_Create_Forms_Automatically_Btn = By.id("ivps-tool-bar-form125batchgenerator-addnew-0");
    public By Auto_Create_125_From_Date = By.cssSelector("#add-form125-batch-generator-datefrom-input .p-inputtext");
    public By Auto_Create_125_To_Date = By.cssSelector("#add-form125-batch-generator-dateto-input .p-inputtext");
    public By Auto_Create_125_Radar_List = By.id("#add-form125-batch-generator-dateto-input .p-inputtext");
    public By Auto_Create_125_Radar_List_child_1 = By.cssSelector("p-dropdownitem:nth-child(1) > .p-dropdown-item");
    public By Auto_Create_125_Radar_List_child_2 = By.cssSelector("p-dropdownitem:nth-child(2) > .p-dropdown-item");
    public By Auto_Create_125_Radar_List_child_3 = By.cssSelector("p-dropdownitem:nth-child(3) > .p-dropdown-item");
    public By Auto_Create_125_Radar_List_child_4 = By.cssSelector("p-dropdownitem:nth-child(4) > .p-dropdown-item");
    public By Auto_Create_125_Radar_List_child_5 = By.cssSelector("p-dropdownitem:nth-child(5) > .p-dropdown-item");
    public By Auto_Create_125_Radar_List_child_6 = By.cssSelector("p-dropdownitem:nth-child(6) > .p-dropdown-item");
    public By Auto_Create_125_Form_Yes_Btn = By.id("add-form125-batch-generator-confirm-button");
    public By Auto_Create_125_Form_No_Btn = By.id("add-form125-batch-generator-cancel-button");
    public By Auto_Create_125_Model_Current_Forms_Btn = By.id("ivps-tool-bar-form125batchgenerator-current-1");
    public By Auto_Create_125_Model_Old_Forms_Btn = By.id("ivps-tool-bar-form125batchgenerator-archived-2");
    public By Auto_Create_125_Model_Total_Forms_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Auto_Create_125_Form_Back_Btn = By.id("breadcrumb-back-button");
    WebDriver driver;

    public E5AutoCreateModel125Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Auto_Create_125_Model_Btn() {
        ElementActions.click(driver, Auto_Create_125_Model_Btn);
    }

    public void Select_Auto_Create_125_Model_Create_Forms_Automatically_Btn() {
        ElementActions.click(driver, Auto_Create_125_Model_Create_Forms_Automatically_Btn);
    }

    public void Type_Auto_Create_125_From_Date() {

        ElementActions.type(driver, Auto_Create_125_From_Date, "2020-01-01");
    }

    public void Type_Auto_Create_125_To_Date() {

        ElementActions.type(driver, Auto_Create_125_To_Date, "2020-12-31");
    }

    public void Select_Auto_Create_125_Radar_List() {
        ElementActions.click(driver, Auto_Create_125_Radar_List);
    }

    public void Select_Auto_Create_125_Radar_List_child_3() {
        ElementActions.click(driver, Auto_Create_125_Radar_List_child_3);
    }

    public void Select_Auto_Create_125_Form_Yes_Btn() {
        ElementActions.click(driver, Auto_Create_125_Form_Yes_Btn);
    }

    public void Select_Auto_Create_125_Form_No_Btn() {
        ElementActions.click(driver, Auto_Create_125_Form_No_Btn);
    }

    public void Select_Auto_Create_125_Model_Current_Forms_Btn() {
        ElementActions.click(driver, Auto_Create_125_Model_Current_Forms_Btn);
    }

    public void Select_Auto_Create_125_Model_Old_Forms_Btn() {
        ElementActions.click(driver, Auto_Create_125_Model_Old_Forms_Btn);
    }

    public void Select_Auto_Create_125_Model_Total_Forms_Count() {
        ElementActions.getElementsCount(driver, Auto_Create_125_Model_Total_Forms_Count);
    }

    public void Select_Auto_Create_125_Form_Back_Btn() {
        ElementActions.click(driver, Auto_Create_125_Form_Back_Btn);
    }
}

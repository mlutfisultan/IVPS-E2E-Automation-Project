package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class F6SkippedViolationsMonitoringPage {
    public By Skipped_Violation_From_Date = By.cssSelector("#form-control-startDate-input .p-inputtext");
    public By Skipped_Violation_To_Date = By.cssSelector("#form-control-endDate-input .p-inputtex");
    public By Skipped_Violation_Username = By.id("form-control-user-input");
    public By Skipped_Violation_Radar_Code = By.id("form-control-radarCode-input");
    public By Skipped_Violation_Submit_Btn = By.id("dynamic-form-submit-form-button");
    public By Skipped_Violation_Total_Forms_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    WebDriver driver;

    public F6SkippedViolationsMonitoringPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Type_Skipped_Violation_From_Date() {
        ElementActions.type(driver, Skipped_Violation_From_Date, "2020-12-01");
    }

    public void Type_Skipped_Violation_To_Date() {
        ElementActions.type(driver, Skipped_Violation_To_Date, "2020-12-01");
    }

    public void Type_Skipped_Violation_Username() {
        ElementActions.type(driver, Skipped_Violation_Username, "super");
    }

    public void Type_Skipped_Violation_Radar_Code() {
        ElementActions.type(driver, Skipped_Violation_Radar_Code, "EGY001");
    }

    public void Select_Skipped_Violation_Submit_Btn() {
        ElementActions.click(driver, Skipped_Violation_Submit_Btn);
    }

    public void Select_Auto_Create_125_Model_Total_Forms_Count() {
        ElementActions.getElementsCount(driver, Skipped_Violation_Total_Forms_Count);
    }
}

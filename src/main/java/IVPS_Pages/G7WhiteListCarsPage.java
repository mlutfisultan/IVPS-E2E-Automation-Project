package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Random;

public class G7WhiteListCarsPage {
    public By White_List_New_Plate_Number_Btn = By.id("ivps-tool-bar-whitelistmanager-addnew-0");
    public By White_List_New_Plate_ID_1 = By.id("cpn1");
    public By White_List_New_Plate_ID_2 = By.id("cpn2");
    public By White_List_New_Plate_ID_3 = By.id("cpn3");
    public By White_List_New_Plate_ID_4 = By.id("cpn4");
    public By White_List_New_Plate_ID_5 = By.id("cpn5");
    public By White_List_Plate_ID_Add_Btn = By.id("add-license-plate-confirm-button");
    public By White_List_Plate_ID_Cancel_Btn = By.id("add-license-plate-cancel-button");
    public By White_List_Display_All_Btn = By.id("ivps-tool-bar-whitelistmanager-showall-1");
    public By White_List_Search_By_Plate_ID = By.id("form-control-plateNumber-input");
    public By White_List_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By White_List_Search_Plate_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By White_List_Confirmation_Message = By.id("toast-container");

    WebDriver driver;

    public G7WhiteListCarsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_White_List_New_Plate_Number_Btn() {
        ElementActions.click(driver, White_List_New_Plate_Number_Btn);
    }

    public void Type_White_List_Add_New_Plate_ID_1() {
        ElementActions.type(driver, White_List_New_Plate_ID_1, getRandomCharacter());
    }

    public void Type_White_List_Add_New_Plate_ID_2() {
        ElementActions.type(driver, White_List_New_Plate_ID_2, getRandomCharacter());
    }

    public void Type_White_List_Add_New_Plate_ID_3() {
        ElementActions.type(driver, White_List_New_Plate_ID_3, getRandomCharacter());
    }

    public void Type_White_List_Add_New_Plate_ID_4() {
        ElementActions.type(driver, White_List_New_Plate_ID_4, getRandomCharacter());
    }

    public void Type_White_List_Add_New_Plate_ID_5() {

        ElementActions.type(driver, White_List_New_Plate_ID_5, getRandomNumber());
    }

    public void Type_White_List_Add_New_Plate_ID() {
        ElementActions.type(driver, White_List_New_Plate_ID_1, getRandomCharacter());
        ElementActions.type(driver, White_List_New_Plate_ID_2, getRandomCharacter());
        ElementActions.type(driver, White_List_New_Plate_ID_3, getRandomCharacter());
//        ElementActions.type(driver, White_List_New_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, White_List_New_Plate_ID_5, getRandomNumber());
    }

    public void Select_White_List_Add_Plate_ID_Btn() {
        ElementActions.click(driver, White_List_Plate_ID_Add_Btn);
    }

    public void Select_White_List_Cancel_Plate_ID_Btn() {
        ElementActions.click(driver, White_List_Plate_ID_Cancel_Btn);
    }

    public void Select_White_List_DisplayAll_Plate_ID_Btn() {
        ElementActions.click(driver, White_List_Display_All_Btn);
    }

    public void Type_White_List_Search_Insert_Plate_ID() {
        ElementActions.type(driver, White_List_Search_By_Plate_ID, "أ ب ت ث 1234");
    }

    public void Select_White_List_Search_Plate_ID_Btn() {
        ElementActions.click(driver, White_List_Search_Btn);
    }

    public void Get_White_List_Search_Plate_Count() {
        ElementActions.getElementsCount(driver, White_List_Search_Plate_Count);
    }
    public String getRandomCharacter() {
        Random random = new Random();
        return ((Character) (char) random.ints(1575, 1610).findAny().getAsInt()).toString();
    }

    public String getRandomNumber() {
        Random random = new Random();
        return random.ints(100, 9999).findAny().getAsInt() + "";
    }
}

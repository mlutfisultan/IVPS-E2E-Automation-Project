package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Random;

public class H8BlackListCarsPage {
    public By Black_List_Cars_New_Plate_Number_Btn = By.id("ivps-tool-bar-whitelistmanager-addnew-0");
    public By Black_List_Cars_New_Plate_ID_1 = By.id("cpn1");
    public By Black_List_Cars_New_Plate_ID_2 = By.id("cpn2");
    public By Black_List_Cars_New_Plate_ID_3 = By.id("cpn3");
    public By Black_List_Cars_New_Plate_ID_4 = By.id("cpn4");
    public By Black_List_Cars_New_Plate_ID_5 = By.id("cpn5");
    public By Black_List_Cars_Add_Plate_ID_Btn = By.id("add-license-plate-confirm-button");
    public By Black_List_Cars_Cancel_Plate_ID_Btn = By.id("add-license-plate-cancel-button");
    public By Black_List_Cars_Display_All_Plate_ID_Btn = By.id("ivps-tool-bar-3d9ee5f4c45bbb231");
    public By Black_List_Cars_Search_By_Plate_ID = By.id("form-control-plateNumber-input");
    public By Black_List_Cars_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Black_List_Cars_Search_Plate_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Black_List_Confirmation_Message = By.id("toast-container");
    WebDriver driver;

    public H8BlackListCarsPage(WebDriver driver) {
        this.driver = driver;
    }


    public void Select_List_Blacklisted_Cars_New_Plate_Number_Btn() {
        ElementActions.click(driver, Black_List_Cars_New_Plate_Number_Btn);
    }

    public void TType_Black_List_Cars_Add_New_Plate_ID_1() {
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_1, getRandomCharacter());
    }

    public void Type_Black_List_Cars_Add_New_Plate_ID_2() {
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_2, getRandomCharacter());
    }

    public void Type_Black_List_Cars_Add_New_Plate_ID_3() {
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_3, getRandomCharacter());
    }

    public void Type_Black_List_Cars_Add_New_Plate_ID_4() {
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_4, getRandomCharacter());
    }

    public void Type_Black_List_Cars_Add_New_Plate_ID_5() {

        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_5, getRandomNumber());
    }

    public void Type_Black_List_Cars_Add_New_Plate_ID() {
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_1, getRandomCharacter());
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_2, getRandomCharacter());
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_3, getRandomCharacter());
//        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, Black_List_Cars_New_Plate_ID_5, getRandomNumber());
    }

    public void Select_List_Blacklisted_Cars_Add_Plate_ID_Btn() {
        ElementActions.click(driver, Black_List_Cars_Add_Plate_ID_Btn);
    }

    public void Select_List_Blacklisted_Cars_Cancel_Plate_ID_Btn() {
        ElementActions.click(driver, Black_List_Cars_Cancel_Plate_ID_Btn);
    }

    public void Select_List_Blacklisted_Cars_DisplayAll_Plate_ID_Btn() {
        ElementActions.click(driver, Black_List_Cars_Display_All_Plate_ID_Btn);
    }

    public void Type_List_Blacklisted_Cars_Search_Insert_Plate_ID() {
        ElementActions.type(driver, Black_List_Cars_Search_By_Plate_ID, "أ ب ت  1234");
    }

    public void Select_List_Blacklisted_Cars_Search_Plate_ID_Btn() {
        ElementActions.click(driver, Black_List_Cars_Search_Btn);
    }

    public void Get_List_Blacklisted_Cars_Search_Plate_Count() {
        ElementActions.getElementsCount(driver, Black_List_Cars_Search_Plate_Count);
    }
    public String getRandomCharacter() {
        Random random = new Random();
        return ((Character) (char) random.ints(1575, 1610).findAny().getAsInt()).toString();
    }

    public String getRandomNumber() {
        Random random = new Random();
        return random.ints(100, 9999).findAny().getAsInt() + "";
    }

}

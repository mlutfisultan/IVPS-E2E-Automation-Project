package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class I9RadarsPage {
    public By Radar_Add_New_Radar_Btn = By.id("ivps-tool-bar-mestamanager-addnew-0");
    public By Radar_Add_New_Radar_Name = By.id("add-edit-view-fusion-fusionCode-input");
    public By Radar_Add_New_Radar_Model = By.cssSelector("#add-edit-view-fusion-model-input .ng-tns-c79-24 p-dropdown-label p-inputtext p-placeholder ng-star-inserted");
    public By Radar_Add_New_Radar_Model_1 = By.cssSelector("p-dropdownitem:nth-child(1) > .p-dropdown-item");
    public By Radar_Add_New_Radar_Model_2 = By.cssSelector("p-dropdownitem:nth-child(2) > .p-dropdown-item");
    public By Radar_Add_New_Radar_Model_3 = By.cssSelector("p-dropdownitem:nth-child(3) > .p-dropdown-item");
    public By Radar_Add_New_Radar_Model_4 = By.cssSelector("p-dropdownitem:nth-child(4) > .p-dropdown-item");
    public By Radar_Add_New_Radar_Location = By.id("add-edit-view-fusion-location-input");
    public By Radar_Add_New_Radar_Longitude = By.id("add-edit-view-fusion-longitude-input");
    public By Radar_Add_New_Radar_Latitude = By.id("add-edit-view-fusion-latitude-input");
    public By Radar_Add_New_Radar_Address = By.id("add-edit-view-fusion-ipAddress-input");
    public By Radar_Add_New_Radar_Add_Btn = By.id("add-edit-view-fusion-confirm-button");
    public By Radar_Add_New_Radar_Cancel_Btn = By.id("add-edit-view-fusion-cancel-button");
    public By Radar_Add_New_Display_All_Btn = By.id("ivps-tool-bar-mestamanager-showall-1");
    public By Radar_Add_New_Search_By_Radar_Code = By.id("form-control-radarCode-input");
    public By Radar_Add_New_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Radar_Add_New_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    WebDriver driver;

    public I9RadarsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Radar_Add_New_Radar_Btn() {
        ElementActions.click(driver, Radar_Add_New_Radar_Btn);
    }

    public void Type_Radar_Add_New_Radar_Name() {
        ElementActions.type(driver, Radar_Add_New_Radar_Name, "IDEMIA Radar Test");
    }

    public void Select_Radar_Add_New_Radar_Model() {
        ElementActions.click(driver, Radar_Add_New_Radar_Model);
    }

    public void Select_Radar_Add_New_Radar_Model_1() {
        ElementActions.click(driver, Radar_Add_New_Radar_Model_1);
    }

    public void Select_Radar_Add_New_Radar_Model_2() {
        ElementActions.click(driver, Radar_Add_New_Radar_Model_2);
    }

    public void Select_Radar_Add_New_Radar_Model_3() {
        ElementActions.click(driver, Radar_Add_New_Radar_Model_3);
    }

    public void Select_Radar_Add_New_Radar_Model_4() {
        ElementActions.click(driver, Radar_Add_New_Radar_Model_4);
    }

    public void Type_Radar_Add_New_Radar_Location() {
        ElementActions.type(driver, Radar_Add_New_Radar_Location, "IDEMIA Radar Test");
    }

    public void Type_Radar_Add_New_Radar_Longitude() {
        ElementActions.type(driver, Radar_Add_New_Radar_Longitude, "100.16088401");
    }

    public void Type_Radar_Add_New_Radar_Latitude() {
        ElementActions.type(driver, Radar_Add_New_Radar_Latitude, "100.16088402");
    }

    public void Type_Radar_Add_New_Radar_Address() {
        ElementActions.type(driver, Radar_Add_New_Radar_Address, "127.0.x.x");
    }

    public void Select_Radar_Add_New_Radar_Add_Btn() {
        ElementActions.click(driver, Radar_Add_New_Radar_Add_Btn);
    }

    public void Select_Radar_Add_New_Radar_Cancel_Btn() {
        ElementActions.click(driver, Radar_Add_New_Radar_Cancel_Btn);
    }

    public void Select_Radar_Add_New_Display_All_Btn() {
        ElementActions.click(driver, Radar_Add_New_Display_All_Btn);
    }

    public void Type_Radar_Add_New_Search_By_Radar_Code() {
        ElementActions.type(driver, Radar_Add_New_Search_By_Radar_Code, "IDEMIA Radar Test");
    }

    public void Select_White_List_Search_Plate_ID_Btn() {
        ElementActions.click(driver, Radar_Add_New_Search_Btn);
    }

    public void Get_White_List_Search_Plate_Count() {
        ElementActions.getElementsCount(driver, Radar_Add_New_Search_Count);
    }
}

package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class J10SystemUserManagementPage {
    public By User_Manage_Add_New_User_Btn = By.id("ivps-tool-bar-usersmanagement-addnew-0");
    public By User_Manage_New_User_Full_Name = By.id("add-edit-user-fullname-input");
    public By User_Manage_New_User_Permission_List = By.cssSelector(".p-checkbox-box:nth-child(2)");
    public By User_Manage_New_User_Permission_All = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_4 = By.cssSelector("p-multiselectitem:nth-child(4)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_5 = By.cssSelector("p-multiselectitem:nth-child(5)  > .p-multiselect-item");
    public By User_Manage_New_User_Name = By.id("add-edit-user-username-input");
    public By User_Manage_New_Password = By.id("add-edit-user-password-input");
    public By User_Manage_New_User_Add_Btn = By.id("add-edit-user-confirm-button");
    public By User_Manage_New_Cancel_Add_Btn = By.id("add-edit-user-cancel-button");
    public By User_Manage_Display_All_Btn = By.id("ivps-tool-bar-usersmanagement-showall-1");
    public By User_Manage_Search_by_Username = By.id("form-control-userName-input");
    public By User_Manage_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By User_Manage_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By User_Manage_Back_Btn = By.id("breadcrumb-back-button");

    WebDriver driver;

    public J10SystemUserManagementPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_User_Manage_Add_New_User_Btn() {
        ElementActions.click(driver, User_Manage_Add_New_User_Btn);
    }

    public void Type_User_Manage_New_User_Full_Name() {
        ElementActions.type(driver, User_Manage_New_User_Full_Name, "IDEMIA IVPS");
    }

    public void Select_User_Manage_New_User_Permission_List() {
        ElementActions.click(driver, User_Manage_New_User_Permission_List);
    }

    public void Select_User_Manage_New_User_Permission_All() {
        ElementActions.click(driver, User_Manage_New_User_Permission_All);
    }

    public void Select_Multi_User_Manage_New_User_Permission() {
        ElementActions.click(driver, User_Manage_New_User_Permission_1);
        ElementActions.click(driver, User_Manage_New_User_Permission_2);
        ElementActions.click(driver, User_Manage_New_User_Permission_3);
        ElementActions.click(driver, User_Manage_New_User_Permission_4);
        ElementActions.click(driver, User_Manage_New_User_Permission_5);
    }

    public void Type_User_Manage_New_User_Name() {
        ElementActions.type(driver, User_Manage_New_User_Name, "IDEMIA_Test");
    }

    public void Type_User_Manage_New_Password() {
        ElementActions.type(driver, User_Manage_New_Password, "IDEMIA_Test");
    }

    public void Select_User_Manage_New_User_Add_Btn() {
        ElementActions.click(driver, User_Manage_New_User_Add_Btn);
    }

    public void Select_User_Manage_New_User_Cancel_Btn() {
        ElementActions.click(driver, User_Manage_New_Cancel_Add_Btn);
    }

    public void Select_User_Manage_Display_All_Btn() {
        ElementActions.click(driver, User_Manage_Display_All_Btn);
    }

    public void Type_User_Manage_Search_by_Username() {
        ElementActions.type(driver, User_Manage_Search_by_Username, "IDEMIA_Test");
    }

    public void Select_User_Manage_Search_Btn() {
        ElementActions.click(driver, User_Manage_Search_Btn);
    }


    public void Get_User_Manage_Search_Count() {
        ElementActions.getElementsCount(driver, User_Manage_Search_Count);
    }

    public void Select_User_Manage_Back_Btn() {
        ElementActions.click(driver, User_Manage_Back_Btn);
    }
}

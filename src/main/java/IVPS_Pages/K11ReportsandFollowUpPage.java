package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class K11ReportsandFollowUpPage {
    //Violations_By_Plate_ID_Report
    public By Violations_By_Plate_ID_Report_Btn = By.id("pages-entry-violations-by-plate-number-report");
    public By Violations_By_Plate_ID = By.id("form-control-plateNumber-input");
    public By Violations_By_Plate_ID_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Violations_By_Plate_ID_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");

    //Violations_Sent_to_PPO_Report
    public By Violations_Sent_to_PPO_Report_Btn = By.id("pages-entry-violations-sent-to-ppo-report");
    public By Violations_Sent_to_PPO_Date_From = By.cssSelector("#form-control-startDate-input .p-inputtext");
    public By Violations_Sent_to_PPO_Date_To = By.cssSelector("#form-control-endDate-input .p-inputtext");
    public By Violations_Sent_to_PPO_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Violations_Sent_to_PPO_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");


    //Violations_By_KPI_Report
    public By Violations_By_KPI_Report_Btn = By.id("pages-entry-users-kpi-report");
    public By Violations_By_KPI_By_Date = By.id("#form-control-date-input .p-inputtext");
    public By Violations_By_KPI_By_Username = By.id("form-control-userName-input");
    public By Violations_By_KPI_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Violations_By_KPI_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");

    //Violations_By_MESTA_Report
    public By Violations_By_MESTA_Report_Btn = By.id("pages-entry-violations-per-mesta-report");
    public By Violations_By_MESTA_Date_From = By.cssSelector("#form-control-startDate-input .p-inputtext");
    public By Violations_By_MESTA_Date_To = By.cssSelector("#form-control-endDate-input .p-inputtext");
    public By Violations_By_MESTA_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Violations_By_MESTA_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");

    //Violations_For_BlackListed_Report
    public By Violations_For_BlackListed_Report_Btn = By.id("pages-entry-violations-for-black-listed-cars-report");
    public By Violations_For_BlackListed_Date_From = By.cssSelector("#form-control-startDate-input .p-inputtext");
    public By Violations_For_BlackListed_Date_To = By.cssSelector("#form-control-endDate-input .p-inputtext");
    public By Violations_For_BlackListed_By_Username = By.id("form-control-userName-input");
    public By Violations_For_BlackListed_By_Plate_ID = By.id("form-control-plateNumber-input");
    public By Violations_For_BlackListed_By_Radar_Code = By.id("form-control-radarCode-input");
    public By Violations_For_BlackListed_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Violations_For_BlackListed_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");

    public By Reports_Followup_Back_Btn = By.id("breadcrumb-back-button");

    WebDriver driver;

    public K11ReportsandFollowUpPage(WebDriver driver) {
        this.driver = driver;
    }

    //Violations_By_Plate_ID_Report
    public void Select_Violations_By_Plate_ID_Report_Btn() {
        ElementActions.click(driver, Violations_By_Plate_ID_Report_Btn);
    }

    public void Type_Violations_By_Plate_ID() {
        ElementActions.type(driver, Violations_By_Plate_ID, "أ ب ت 1234");
    }

    public void Select_Violations_By_Plate_ID_Search_Btn() {
        ElementActions.click(driver, Violations_By_Plate_ID_Search_Btn);
    }

    public void Get_Violations_By_Plate_ID_Search_Count() {
        ElementActions.getElementsCount(driver, Violations_By_Plate_ID_Search_Count);
    }


    //Violations_Sent_to_PPO_Report
    public void Select_Violations_Sent_to_PPO_Report_Btn() {
        ElementActions.click(driver, Violations_Sent_to_PPO_Report_Btn);
    }

    public void Type_Violations_Sent_to_PPO_Date_From() {
        ElementActions.type(driver, Violations_Sent_to_PPO_Date_From, "01/01/2020");
    }

    public void Type_Violations_Sent_to_PPO_Date_To() {
        ElementActions.type(driver, Violations_Sent_to_PPO_Date_To, "28/02/2022");
    }

    public void Select_Violations_Sent_to_PPO_Search_Btn() {
        ElementActions.click(driver, Violations_Sent_to_PPO_Search_Btn);
    }

    public void Get_Violations_Sent_to_PPO_Search_Count() {
        ElementActions.getElementsCount(driver, Violations_Sent_to_PPO_Search_Count);
    }

    //Violations_By_KPI_Report
    public void Select_Violations_By_KPI_Report_Btn() {
        ElementActions.click(driver, Violations_By_KPI_Report_Btn);
    }

    public void Type_Violations_By_KPI_By_Date() {
        ElementActions.type(driver, Violations_By_KPI_By_Date, "01/01/2020");
    }

    public void Type_Violations_By_KPI_By_Username() {
        ElementActions.type(driver, Violations_By_KPI_By_Username, "IDEMIA_Test");
    }

    public void Select_Violations_By_KPI_Search_Btn() {
        ElementActions.click(driver, Violations_By_KPI_Search_Btn);
    }

    public void Get_Violations_By_KPI_Search_Count() {
        ElementActions.getElementsCount(driver, Violations_By_KPI_Search_Count);
    }


    //Violations_By_MESTA_Report
    public void Select_Violations_By_MESTA_Report_Btn() {
        ElementActions.click(driver, Violations_By_MESTA_Report_Btn);
    }

    public void Type_Violations_By_MESTA_Date_From() {
        ElementActions.type(driver, Violations_By_MESTA_Date_From, "01/01/2020");
    }

    public void Type_Violations_By_MESTA_Date_To() {
        ElementActions.type(driver, Violations_By_MESTA_Date_To, "28/02/2022");
    }

    public void Select_Violations_By_MESTA_Search_Btn() {
        ElementActions.click(driver, Violations_By_MESTA_Search_Btn);
    }

    public void Get_Violations_By_MESTA_Search_Count() {
        ElementActions.getElementsCount(driver, Violations_By_MESTA_Search_Count);
    }


    //Violations_For_BlackListed_Report
    public void Select_Violations_For_BlackListed_Report_Btn() {
        ElementActions.click(driver, Violations_For_BlackListed_Report_Btn);
    }

    public void Type_Violations_For_BlackListed_Date_From() {
        ElementActions.type(driver, Violations_For_BlackListed_Date_From, "01/01/2020");
    }

    public void Type_Violations_For_BlackListed_Date_To() {
        ElementActions.type(driver, Violations_For_BlackListed_Date_To, "28/02/2022");
    }

    public void Type_Violations_For_BlackListed_By_Username() {
        ElementActions.type(driver, Violations_For_BlackListed_By_Username, "IDEMIA_Test");
    }

    public void Type_Violations_For_BlackListed_By_Plate_ID() {
        ElementActions.type(driver, Violations_For_BlackListed_By_Plate_ID, "أ ب ت 1234");
    }

    public void Type_Violations_For_BlackListed_By_Radar_Code() {
        ElementActions.type(driver, Violations_For_BlackListed_By_Radar_Code, "EGY001");
    }

    public void Select_Violations_For_BlackListed_Search_Btn() {
        ElementActions.click(driver, Violations_For_BlackListed_Search_Btn);
    }

    public void Get_Violations_For_BlackListed_Search_Count() {
        ElementActions.getElementsCount(driver, Violations_For_BlackListed_Search_Count);
    }

    public void Select_Reports_Followup_Back_Btn() {
        ElementActions.click(driver, Reports_Followup_Back_Btn);
    }
}

package IVPS_Pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class N14SystemSettingsPage {
    public By Sys_Technical_Settings_Btn = By.id("pages-entry-technical");
    public By Sys_Technical_Settings_Page_Label = By.cssSelector("#breadcrumb-technical > span");
    public By Sys_General_Settings_Btn = By.id("pages-entry-business");
    public By Sys_General_Settings_Page_Label = By.cssSelector("#breadcrumb-business > span");
    public By Sys_Settings_Back_Btn = By.id("breadcrumb-back-button");

    WebDriver driver;

    public N14SystemSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Sys_Technical_Settings_Btn() {
        ElementActions.click(driver, Sys_Technical_Settings_Btn);
    }

    public void Select_Sys_General_Settings_Btn() {
        ElementActions.click(driver, Sys_General_Settings_Btn);
    }

    public void Select_Sys_Settings_Back_Btn() {
        ElementActions.click(driver, Sys_Settings_Back_Btn);
    }
}

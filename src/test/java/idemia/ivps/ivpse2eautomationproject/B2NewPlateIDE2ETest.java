package idemia.ivps.ivpse2eautomationproject;

import IVPS_Pages.A1MainPage;
import IVPS_Pages.B2WelcomePage;
import IVPS_Pages.C3Form125Page;
import com.shaft.gui.element.ElementActions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class B2NewPlateIDE2ETest extends TestBase {
    private A1MainPage MainPage_Obj;
    private B2WelcomePage WelcomePage_Obj;
    private C3Form125Page Form125Page_Obj;

    @Test(description = "TS001 || Login to IVPS", priority = 0)
    public void IVPS_Login() {
        MainPage_Obj = new A1MainPage(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new B2WelcomePage(driver);
        String Welcome_Page = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText().contains("القائمة الرئيسية"));
        System.out.println("Page Label name is" + Welcome_Page);
    }

    @Test(description = "TS002 || Create and Submit 125 Form", priority = 1)
    public void Create_and_Submit_125_Form() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Form125Page_Obj = new C3Form125Page(driver);
        Form125Page_Obj.Fill_inputs_View_Violartions();
        Form125Page_Obj.Create_125_Form_New_Plate_ID();
        ElementActions.waitForElementToBePresent(driver, Form125Page_Obj.Form_125_Plate_ID_1, 3000, true);
    }

}

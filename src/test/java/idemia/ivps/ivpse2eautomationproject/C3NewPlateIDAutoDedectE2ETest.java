package idemia.ivps.ivpse2eautomationproject;

import IVPS_Pages.A1MainPage;
import IVPS_Pages.B2WelcomePage;
import IVPS_Pages.C3Form125Page;
import com.shaft.gui.element.ElementActions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class C3NewPlateIDAutoDedectE2ETest extends TestBase {
    private A1MainPage MainPage_Obj;
    private B2WelcomePage WelcomePage_Obj;
    private C3Form125Page Form125Page_Obj;


    @Test(description = "TS001 || Login to IVPS", priority = 0)
    public void IVPS_Login() {
        MainPage_Obj = new A1MainPage(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new B2WelcomePage(driver);
        String Welcome_Page = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText().contains("القائمة الرئيسية"));
        System.out.println("Page Lable name is" + Welcome_Page);
    }

    @Test(description = "TS002 || Create and Submit 125 Form", priority = 1)
    public void Create_and_Submit_125_Form() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Form125Page_Obj = new C3Form125Page(driver);
        Form125Page_Obj.Select_Form_125_Generator_Btn();
        Form125Page_Obj.Select_Form_125_Arrangement_Type();
        Form125Page_Obj.Select_Form_125_Arrangement_Type_Dsc();
        Form125Page_Obj.Type_Form_125_Violation_From_Date();
        Form125Page_Obj.Select_Form_125_View_Violations_Btn();
        ElementActions.waitForElementToBePresent(driver, Form125Page_Obj.Form_125_Plate_ID_1, 3000, true);
        Form125Page_Obj.Select_Form_125_Type_of_Offense();
        Form125Page_Obj.Select_List_Of_Form_125_Type_Offense_Child();
        Form125Page_Obj.Select_Offenses_List_closer_Btn();
        Form125Page_Obj.Select_Form_125_Creation_Btn();
        ElementActions.waitForElementToBePresent(driver, Form125Page_Obj.Form_125_Plate_ID_1, 3000, true);
    }

}

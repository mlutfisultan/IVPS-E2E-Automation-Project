package idemia.ivps.ivpse2eautomationproject;

import IVPS_Pages.A1MainPage;
import IVPS_Pages.B2WelcomePage;
import IVPS_Pages.D4SearchInViolationsPage;
import com.shaft.validation.Assertions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class D4SearchForViolationByNewPlate extends TestBase {
    private A1MainPage MainPage_Obj;
    private B2WelcomePage WelcomePage_Obj;
    private D4SearchInViolationsPage SearchInViolationsPage_Obj;

    @Test(description = "TS001 || Login to IVPS", priority = 0)
    public void IVPS_Login() {
        MainPage_Obj = new A1MainPage(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new B2WelcomePage(driver);
        String Welcome_Page = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText().contains("القائمة الرئيسية"));
        System.out.println("Page Lable name is" + Welcome_Page);
    }

    @Test(description = "TS002 || Search_For_Violations_With_New_Plate", priority = 1)
    public void Search_For_Violations_With_New_Plate() {
        String PlateID = "ص م س 286";
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SearchInViolationsPage_Obj = new D4SearchInViolationsPage(driver);
        SearchInViolationsPage_Obj.Select_Serach_Violations_Btn();
        Assertions.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Search_For_Violations_Label).isDisplayed());
        SearchInViolationsPage_Obj.Search_By_New_Plate_ID(PlateID);
        Assertions.assertEquals(PlateID, driver.findElement(SearchInViolationsPage_Obj.Search_For_Violations_Search_Result).getText());
    }
}

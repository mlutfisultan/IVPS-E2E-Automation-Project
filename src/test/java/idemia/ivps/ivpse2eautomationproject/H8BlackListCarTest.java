package idemia.ivps.ivpse2eautomationproject;

import IVPS_Pages.A1MainPage;
import IVPS_Pages.B2WelcomePage;
import IVPS_Pages.H8BlackListCarsPage;
import org.testng.annotations.Test;

public class H8BlackListCarTest extends TestBase {
    private A1MainPage MainPage_Obj;
    private B2WelcomePage WelcomePage_Obj;
    private H8BlackListCarsPage BlackListPage_Obj;

    @Test(description = "TS001 || Login to IVPS", priority = 0)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
    }


    @Test(description = "TS002 || Add and Search BlackListed Car", priority = 1)
    public void Search_and_Add_BlackListCar() {
        // Search for blacklisted car
        WelcomePage_Obj.Select_Black_List_Cars_Btn();
        BlackListPage_Obj = new H8BlackListCarsPage(driver);
        BlackListPage_Obj.Type_List_Blacklisted_Cars_Search_Insert_Plate_ID();
        BlackListPage_Obj.Select_List_Blacklisted_Cars_Search_Plate_ID_Btn();
        BlackListPage_Obj.Get_List_Blacklisted_Cars_Search_Plate_Count();
        BlackListPage_Obj.Select_List_Blacklisted_Cars_DisplayAll_Plate_ID_Btn();
        //Add blacklisted car
        BlackListPage_Obj.Select_List_Blacklisted_Cars_New_Plate_Number_Btn();
        BlackListPage_Obj.Type_Black_List_Cars_Add_New_Plate_ID();
        BlackListPage_Obj.Select_List_Blacklisted_Cars_Add_Plate_ID_Btn();

    }

}

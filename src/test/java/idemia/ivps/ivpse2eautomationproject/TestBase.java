package idemia.ivps.ivpse2eautomationproject;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestBase {
    protected WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = BrowserFactory.getBrowser(DriverFactory.DriverType.DESKTOP_CHROME);
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
    }

    @AfterClass
    public void tearDown() {
      /*  WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);*/
    }
}

